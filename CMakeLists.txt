cmake_minimum_required(VERSION 3.13)
project(queue)

find_package(GTest REQUIRED)
find_package(Threads REQUIRED)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED on)

SET(CMAKE_CXX_FLAGS -pthread)

include_directories(${GTEST_INCLUDE_DIR})

add_executable(queue src/main.cpp src/Queue.hpp src/QueueProcessor.hpp )

enable_testing()
add_test(TestSerialization "./TestSerialization")
target_link_libraries(queue gtest gtest_main)
