#ifndef QUEUE_QUEUE_HPP
#define QUEUE_QUEUE_HPP

#include <iostream>
#include <queue>
#include <functional>
#include <mutex>
#include <list>
#include <thread>
#include <shared_mutex>

namespace project
{
    template < typename TYPE >
    class Queue
    {
    private:
        std::queue<std::shared_ptr<TYPE>> _data;

    private:
        mutable std::mutex _mutex;

        std::condition_variable _cond;

    public:
        void push(TYPE && value)
        {
            auto data = std::make_shared<TYPE>(value);
            std::lock_guard<std::mutex> lock(_mutex);

            _data.push(data);
            _cond.notify_one();
        }

        void push(TYPE & value)
        {
            auto data = std::make_shared<TYPE>(std::move(value));
            std::lock_guard<std::mutex> lock(_mutex);

            _data.push(data);
            _cond.notify_one();
        }

        std::shared_ptr<TYPE> wait_and_pop()
        {
            return wait_and_pop([] { return true; });
        }

        std::shared_ptr<TYPE> wait_and_pop(const std::function<bool()> & additionalCondition)
        {
            std::unique_lock<std::mutex> lock(_mutex);

            _cond.wait(lock,
                       [this, &additionalCondition]
                       {
                           const auto isEmpty = _data.empty();
                           if (isEmpty && !additionalCondition())
                               return true;

                           return !isEmpty;
                       });

            if (_data.empty())
                return nullptr;

            auto value = _data.front();
            _data.pop();
            return value;
        }

        std::shared_ptr<TYPE> pop()
        {
            std::lock_guard<std::mutex> lock(_mutex);

            if (_data.empty())
                return nullptr;

            auto value = _data.front();
            _data.pop();
            return value;
        }

        size_t size() const
        {
            std::unique_lock<std::mutex> lock(_mutex);
            return _data.size();
        }

        bool empty() const
        {
            std::lock_guard<std::mutex> lock(_mutex);
            return _data.empty();
        }

        void unlockWait()
        {
            _cond.notify_one();
        }
    };

}
#endif //QUEUE_QUEUE_HPP
