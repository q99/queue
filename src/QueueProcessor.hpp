#ifndef QUEUE_QUEUEPROCESSOR_HPP
#define QUEUE_QUEUEPROCESSOR_HPP

#include <iostream>
#include <queue>
#include <functional>
#include <mutex>
#include <list>
#include <thread>
#include <shared_mutex>
#include <future>

#include "Queue.hpp"

namespace project
{
    template < typename Iterator, typename Function >
    void parallel_for_each(Iterator begin, Iterator end, Function func)
    {
        const unsigned long distance = std::distance(begin, end);

        if (!distance)
            return;

        const uint data_in_thread = 3;

        if (distance < data_in_thread)
        {
            std::for_each(begin, end, func);
            return;
        }

        auto middle = begin + (distance / 2);

        auto half = std::async(&parallel_for_each<Iterator, Function>, begin, middle, func);
        parallel_for_each(middle, end, func);
        half.get();
    }

    template < typename TYPE >
    class QueueProcessor
    {
    public:
        using Callback = std::function<void(const TYPE &)>;
        using Source = std::function<std::unique_ptr<TYPE>(void)>;

    public:
        QueueProcessor(const std::initializer_list<Source> & sources,
                       const std::initializer_list<Callback> & callbacks) :
                _callbacks(callbacks),
                _sources(sources),
                _isCallSources(false)
        {}

        void start()
        {
            _isCallSources = true;
            _sourceThread = std::thread([this] { startSources(); });
            _callbackThread = std::thread([this] { startCallbacks(); });
        }

        void stop()
        {
            _isCallSources = false;

            _sourceThread.join();
            _queue.unlockWait();
            _callbackThread.join();
        }

    private:
        void startCallbacks()
        {
            while (_sourceThread.joinable() || !_queue.empty())
            {
                auto value = _queue.wait_and_pop([this]{ return _sourceThread.joinable(); });

                if (value != nullptr)
                    parallel_for_each(_callbacks.begin(), _callbacks.end(),
                                      [&value](const auto & callback)
                                      {
                                          callback(*value);
                                      });
            }
        }

        void startSources()
        {
            while (_isCallSources)
            {
                parallel_for_each(_sources.begin(), _sources.end(),
                                  [this](const auto & source)
                                  {
                                      if (auto val = source(); val != nullptr)
                                      {
                                          _queue.push(*val);
                                      }
                                  });

                std::this_thread::sleep_for(std::chrono::microseconds(10));
            }
        }


    private:
        Queue <TYPE> _queue;
        bool _isCallSources{};

        std::vector<Callback> _callbacks;
        std::vector<Source> _sources;

    private:
        std::thread _sourceThread;
        std::thread _callbackThread;
    };
}

#endif //QUEUE_QUEUEPROCESSOR_HPP
