#include <random>

#include "../src/Queue.hpp"
#include "gtest/gtest.h"
#include "QueueProcessor.hpp"

using namespace project;

TEST(Queue, Push)
{
    Queue<int> queue;
    queue.push(1);
    int val = 10;
    queue.push(val);
    ASSERT_EQ(*queue.pop(), 1);
    ASSERT_EQ(*queue.pop(), val);
}

TEST(Queue, EmptyPop)
{
    ASSERT_EQ(Queue<int>().pop(), nullptr);
}

TEST(Queue, Empty)
{
    ASSERT_TRUE(Queue<int>().empty());
}

TEST(Queue, Simple)
{
    int count = 1000, ptr = count;
    Queue<int> queue;

    while (ptr-- != 0)
        queue.push(ptr);

    ASSERT_EQ(queue.size(), count);

    ptr = count;
    while (ptr-- != 0)
    {
        ASSERT_EQ(ptr, *queue.pop());
    }
}

TEST(Queue, Thread)
{
    Queue<int> queue;
    bool flag = false;
    std::thread([&queue, &flag]()
                {
                    std::this_thread::sleep_for(std::chrono::seconds(1));
                    flag = true;
                    queue.push(10);
                }).detach();

    ASSERT_EQ(10, *queue.wait_and_pop());
    ASSERT_TRUE(flag);
}

TEST(QueueProcess, Simple)
{
    int limit = 1005, ptr = 0, seconds_wait = 10;
    std::mutex lock;

    auto source =
            [&limit, &ptr, &lock]() -> std::unique_ptr<int>
            {
                std::lock_guard<std::mutex> _lock(lock);
                std::this_thread::sleep_for(std::chrono::seconds(1));

                if (ptr == limit)
                    return nullptr;

                return std::make_unique<int>(ptr++);
            };

    std::set<int> result1, result2;

    auto callback1 =
            [&result1](const int & value) -> void
            {
                result1.insert(value);
            };

    auto callback2 =
            [&result2](const auto & value) -> void
            {
                result2.insert(value);
            };

    auto process = QueueProcessor<int>({source, source, source}, {callback1, callback2});
    process.start();
    std::this_thread::sleep_for(std::chrono::seconds(seconds_wait));
    process.stop();

    ASSERT_EQ(result1, result2);
    const auto max = *std::max_element(result1.cbegin(), result1.cend());
    ASSERT_EQ(result1.size(), max + 1);
    ASSERT_EQ(max, ptr - 1);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}